#!/bin/sh
 
# homebrew-cask
brew tap phinze/homebrew-cask
brew install brew-cask
brew tap caskroom/versions
 
# browsers
# brew cask install google-chrome-canary
# brew cask install google-chrome
brew cask install firefox
brew cask install firefox-nightly --force
brew cask install webkit-nightly --force
brew cask install chromium --force
# brew cask install torbrowser

# daily
brew cask install dropbox
brew cask install evernote
brew cask install google-drive
# brew cask install rescuetime

# development
brew cask install java
brew cask install atom
brew cask install balsamiq-mockups
brew cask install mysqlworkbench
brew cask install sequel-pro
brew cask install sublime-text
# brew cask install visual-studio-code
brew cask install filezilla
# brew cask install kaleidoscope
brew cask install beyond-compare
brew cask install iterm2
brew cask install sourcetree
brew cask install vagrant
brew cask install virtualbox
# brew cask install vmware-fusion
brew cask install webstorm
 
# other
# brew cask install alfred
# brew cask alfred link
# brew cask install dropbox
brew cask install macdown
brew cask install skype
# brew cask install gitter
# brew cask install slack
# brew cask install spotify
brew cask install dash 
# brew cask install evernote
brew cask install expandrive
brew cask install caffeine
brew cask install 1password

